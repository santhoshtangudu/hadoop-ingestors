package com.amrita.dataingestor.examples;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;

import java.io.*;
import java.util.Properties;

public class SingleThreadedKafkaIngestor {
    private KafkaProducer<String, String> kafkaProducer;

    SingleThreadedKafkaIngestor(String brokerAddress) {
        //Create KafkaProducer
        Properties props = new Properties();
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, brokerAddress);
        props.put(ProducerConfig.CLIENT_ID_CONFIG, "Kafka-Single-Thread-Ingestor");
        props.put(ProducerConfig.ACKS_CONFIG, "1");
        props.put(ProducerConfig.RETRIES_CONFIG, 0);
        props.put(ProducerConfig.LINGER_MS_CONFIG, 0);

        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        props.put(ProducerConfig.PARTITIONER_CLASS_CONFIG, CustomPartitioner.class.getName());

        this.kafkaProducer =  new KafkaProducer<String, String>(props);
    }

    private void ingestToKafka(String key, String record, String topicName) {
        ProducerRecord<String, String> producerRecord = new ProducerRecord<>(topicName, key, record);
        this.kafkaProducer.send(producerRecord);
    }

    private void closeProducer() {
        this.kafkaProducer.close();
    }

    public static void main(String[] args) {

        String fileName = args[0];
        String topicName = args[1];
        String brokerAddress = args[2];

        String line;
        File file = new File(fileName);
        SingleThreadedKafkaIngestor singleThreadKafkaIngestor = new SingleThreadedKafkaIngestor(brokerAddress);
        BufferedReader br = null;
        try {
            FileReader fr = new FileReader(file);
            br = new BufferedReader(fr);
            while ((line = br.readLine()) != null ) {
                String[] columns = line.split("\\t");
                singleThreadKafkaIngestor.ingestToKafka(columns[2], line, topicName);
            }

        } catch (FileNotFoundException ex) {
            System.out.println("Couldn't find the file. Unable to ingest the data. " + ex.getStackTrace());
        } catch (IOException ex) {
            System.out.println("IO exception. Unable to ingest the data. " + ex.getStackTrace());
        }finally {
            try {
                br.close();
                singleThreadKafkaIngestor.closeProducer();
            } catch (Exception ex) {
                System.out.println("Unable to close the connections" + ex.getStackTrace());
            }

        }
    }
}
