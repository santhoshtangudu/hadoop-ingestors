package com.amrita.dataingestor.examples;

import org.apache.kafka.clients.producer.Partitioner;
import org.apache.kafka.common.Cluster;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

public class CustomPartitioner implements Partitioner {
    private static final Logger logger = LoggerFactory.getLogger(CustomPartitioner.class);

    @Override
    public int partition(String topic, Object key, byte[] keyBytes, Object value, byte[] valueBytes, Cluster cluster) {
        if (key != null) {
            String ky = key.toString()  ;
            if(ky.trim().equals("activation")) {
                return 0;
            } else if(ky.equals("binding")) {
                return 1;
            } else if(ky.equals("catalysis")) {
                return 2;
            } else if(ky.equals("expression")) {
                return 3;
            } else if(ky.equals("inhibition")) {
                return 4;
            } else if(ky.equals("ptmod")) {
                return 5;
            } else if(ky.equals("reaction")) {
                return 6;
            }
        }
        return 7;
    }

    @Override
    public void close() {

    }

    @Override
    public void configure(Map<String, ?> configs) {

    }

}
