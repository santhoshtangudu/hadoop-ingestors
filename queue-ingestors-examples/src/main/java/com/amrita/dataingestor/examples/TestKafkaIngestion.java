package com.amrita.dataingestor.examples;

import com.amrita.hadoopdata.ingestor.KafkaOutputFormat;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.util.GenericOptionsParser;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.apache.kafka.clients.producer.Partitioner;
import org.apache.kafka.common.Cluster;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Map;

public class TestKafkaIngestion extends Configured implements Tool {
    public int run(String[] args) throws Exception {
        Configuration conf = getConf();

//        conf.set("mapreduce.tasktracker.map.tasks.maximum", "4");
        args = new GenericOptionsParser(conf, args).getRemainingArgs();

        Job job = new Job(conf, "Test Kafka Ingestor");
        job.setJarByClass(TestKafkaIngestion.class);

        job.setMapperClass(TSVKafkaMapper.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);

        job.setNumReduceTasks(0);
        job.setOutputFormatClass(KafkaOutputFormat.class);

        FileInputFormat.addInputPath(job, new Path(args[0]));
        System.exit(job.waitForCompletion(true) ? 0: 1);

        return 0;
    }

    //Command to execute this code:  hadoop jar target/queue-ingestors-examples-1.0-SNAPSHOT-jar-with-dependencies.jar com.amrita.dataingestor.examples.TestKafkaIngestion  -Dmapred.kafka.bootstrap.servers=localhost:9092 -Dmapred.kafka.topic=string-kafka-sampleq  <input_location>
    public static void main(String[] args) throws Exception {
        System.out.println(CustomPartitioner.class.getName());
        int res = ToolRunner.run(new Configuration(), new TestKafkaIngestion(), args);
        System.exit(res);
    }
}

final class TSVKafkaMapper extends Mapper<LongWritable, Text, Text, Text> {
    private static final Logger logger = LoggerFactory.getLogger(TSVKafkaMapper.class);

    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        try {
            if(value != null) {
                String[] columns = value.toString().split("\\t");
                context.write(new Text(columns[2]), value);
            }
        } catch (Exception ex) {
            logger.error("Splitting has issue. " + value.toString());
        }
    }
}
