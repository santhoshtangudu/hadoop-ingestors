package com.amrita.dataingestor.examples;

import com.amrita.hadoopdata.ingestor.KafkaOutputFormat;
import com.amrita.hadoopdata.ingestor.RabbitMQOutputFormat;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.util.GenericOptionsParser;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class TestRabbitMQIngestion extends Configured implements Tool {
    public int run(String[] args) throws Exception {
        Configuration conf = getConf();

        args = new GenericOptionsParser(conf, args).getRemainingArgs();

        Job job = new Job(conf, "Test RabbitMQ Ingestor");
        job.setJarByClass(TestRabbitMQIngestion.class);

        job.setMapperClass(TSVRabbitMQMapper.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);

        job.setNumReduceTasks(0);
        job.setOutputFormatClass(RabbitMQOutputFormat.class);

        FileInputFormat.addInputPath(job, new Path(args[0]));
        System.exit(job.waitForCompletion(true) ? 0: 1);

        return 0;
    }

    //Command to execute this code:
    public static void main(String[] args) throws Exception {
        int res = ToolRunner.run(new Configuration(), new TestRabbitMQIngestion(), args);
        System.exit(res);
    }
}

final class TSVRabbitMQMapper extends Mapper<LongWritable, Text, Text, Text> {
    private static final Logger logger = LoggerFactory.getLogger(TSVRabbitMQMapper.class);

    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        try {
            if(value != null) {
                String[] columns = value.toString().split("\\t");
                context.write(new Text(columns[2]), value);
            }
        } catch (Exception ex) {
            logger.error("Splitting has issue. " + value.toString());
        }
    }
}


