package com.amrita.dataingestor.examples;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.*;
import java.util.concurrent.TimeoutException;

public class SingleThreadedRabbitMQIngestor {
    private Channel channel;
    private Connection connection;


    SingleThreadedRabbitMQIngestor(String rabbitMQHost, String userName, String password) {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(rabbitMQHost);
        factory.setUsername(userName);
        factory.setPassword(password);

        try {
            this.connection = factory.newConnection();
            this.channel = connection.createChannel();

        } catch (TimeoutException ex) {

        } catch (IOException ex) {

        }
    }

    private void ingestToRabbitMQ(String exchangeName, String key, String record) {
        try {
            this.channel.basicPublish(exchangeName, key, null, record.getBytes("UTF-8"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void closeProducer() {
        try {
            channel.close();
            connection.close();
        } catch (TimeoutException e) {
            e.printStackTrace();
        } catch (IOException e) {

        }
    }

    public static void main(String[] args) {
        String fileName = args[0];
        String exchangeName = args[1];
        String rabbitMQHost = args[2];
        String userName = args[3];
        String password = args[4];

        String line;
        File file = new File(fileName);
        SingleThreadedRabbitMQIngestor singleThreadRabbitMQIngestor = new SingleThreadedRabbitMQIngestor(rabbitMQHost, userName, password);
        BufferedReader br = null;
        try {
            FileReader fr = new FileReader(file);
            br = new BufferedReader(fr);
            while ((line = br.readLine()) != null ) {
                String[] columns = line.toString().split("\\t");
                singleThreadRabbitMQIngestor.ingestToRabbitMQ(exchangeName, columns[2], line);
            }
        } catch (FileNotFoundException ex) {
            System.out.println("Couldn't find the file. Unable to ingest the data. " + ex.getStackTrace());
        } catch (IOException ex) {
            System.out.println("IO exception. Unable to ingest the data. " + ex.getStackTrace());
        }finally {
            try {
                br.close();
                singleThreadRabbitMQIngestor.closeProducer();
            } catch (Exception ex) {
                System.out.println("Unable to close the connections" + ex.getStackTrace());
            }

        }
    }
}
