package com.amrita.dataingestor.examples;

public class MaxSumTreeProblem {
//    private static int sumOfGrandChildren(Node node) {
//        return 0;
//    }
//
//    private static int getMaxSum(Node node) {
//        if (node == null) {
//            return 0;
//        }
//        int inclSum = node.data + sumOfGrandChildren(node);
//
//        int excludSum = getMaxSum(node.left) + getMaxSum(node.right);
//
//    }

    public static void main(String[] args) {
        Node head = new Node(1);
        head.left = new Node(2);
        head.left.left = new Node(6);
        head.right = new Node(3);
        head.right.left = new Node(4);
        head.right.right = new Node(5);
//        System.out.println(getMaxSum(head));
    }
}

class Node {
    int data;
    Node left, right;
    Node(int data) {
        this.data = data;
    }
}
