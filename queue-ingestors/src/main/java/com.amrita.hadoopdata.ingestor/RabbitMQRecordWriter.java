package com.amrita.hadoopdata.ingestor;

import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import org.apache.hadoop.mapreduce.RecordWriter;
import org.apache.hadoop.mapreduce.TaskAttemptContext;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * RabbitMQRecordWriter is helpful to write the data to RabbitMQ cluster.
 * @param <K>
 * @param <V>
 */
public class RabbitMQRecordWriter<K,V> extends RecordWriter<K, V> {
    private Connection connection;
    private Channel channel;
    private String exchange;

    public RabbitMQRecordWriter(Connection connection, String rabbitmqExchange) {
        try {
            this.connection = connection;
            this.channel = connection.createChannel();
            this.exchange = rabbitmqExchange;
        } catch (IOException ex) {

        }
    }

    /**
     * write method writes the data to RabbitMQ exchange. Later exchange will decide where to pass the message based on
     * routing key and exchange type.
     * @param key
     * @param value
     * @throws IOException
     * @throws InterruptedException
     */
    @Override
    public void write(K key, V value) throws IOException, InterruptedException {
        channel.basicPublish(this.exchange, key.toString(), null, value.toString().getBytes("UTF-8"));
    }

    /**
     * Closes the the connection after completion of write operation.
     * @param context
     * @throws IOException
     * @throws InterruptedException
     */
    @Override
    public void close(TaskAttemptContext context) throws IOException, InterruptedException {
        try {
            channel.close();
            connection.close();
        } catch (TimeoutException e) {
            e.printStackTrace();
        }
    }
}
