package com.amrita.hadoopdata.ingestor;

import org.apache.hadoop.mapreduce.RecordWriter;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * KafkaRecordWriter is helpful to write the data to Kafka Cluster.
 * @param <K> Key(of type K) is used for deciding partition Id
 * @param <V> Value (of type V) is actual message
 */
public class KafkaRecordWriter<K,V> extends RecordWriter<K, V> {
    private static Logger logger = LoggerFactory.getLogger(KafkaRecordWriter.class);
    private KafkaProducer kafkaProducer;
    private String topic;

    public KafkaRecordWriter(KafkaProducer kafkaProducer, String topic) {
        this.kafkaProducer = kafkaProducer;
        this.topic = topic;
    }

    /**
     * write method writes the data to kafka.
     * @param key
     * @param value
     * @throws IOException
     * @throws InterruptedException
     */
    @Override
    public void write(K key, V value) throws IOException, InterruptedException {
        logger.debug("The record is as follows. key: " + key.toString() + "  value: "+ value.toString());
        ProducerRecord<K, V> record;
        if(key != null) {
             record = new ProducerRecord<K, V>(this.topic, key, value);
        } else {
            record = new ProducerRecord<K, V>(this.topic, value);
        }
        this.kafkaProducer.send(record);
    }

    /**
     * Closes the KafkaProducer after completion of write
     * @param context
     * @throws IOException
     * @throws InterruptedException
     */
    @Override
    public void close(TaskAttemptContext context) throws IOException, InterruptedException {
        this.kafkaProducer.close();
    }
}
