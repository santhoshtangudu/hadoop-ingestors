package com.amrita.hadoopdata.ingestor;

import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.mapreduce.*;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 *  RabbitMQOutputFormat is the class which helps to ingest data to RabbitMQ. This class should set as OutputFormat in Hadoop
 *  MapReduce Job to redirect the messages to RabbitMQ exchange.
 * @param <K> Key(of type K) is used as routing key
 * @param <V> Value(of type V) is actual message
 */
public class RabbitMQOutputFormat<K, V>  extends OutputFormat<K, V> {
    private static final String RABBITMQ_SERVERS = "mapred.rabbitmq.servers";
    private static final String RABBITMQ_EXCHANGE = "mapred.rabbitmq.exchange";
    private static final String RABBITMQ_USERNAME = "mapred.rabbitmq.user";
    private static final String RABBITMQ_PASSWORD = "mapred.rabbitmq.password";

    /**
     * Returns RabbitMQRecordWriter object. Here we set the default values for all the parameters except rabbitmqhost,
     * rabbitmq exchange and it's type. Here, user given parameters overwrite the default parameters. If user wants to
     * set different values than defaults, those can be set here.
     * @param context
     * @return
     * @throws IOException
     * @throws InterruptedException
     */
    @Override
    public RecordWriter<K, V> getRecordWriter(TaskAttemptContext context) throws IOException, InterruptedException {
        Configuration conf = context.getConfiguration();

        //Read the configuration from the user
        String rabbitmqHost = conf.get(RABBITMQ_SERVERS);
        String rabbitmqExchange = conf.get(RABBITMQ_EXCHANGE);
        String userName = conf.get(RABBITMQ_USERNAME);
        String password = conf.get(RABBITMQ_PASSWORD);

        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(rabbitmqHost);
        factory.setUsername(userName);
        factory.setPassword(password);
        try {
            Connection connection = factory.newConnection();
            return new RabbitMQRecordWriter<>(connection, rabbitmqExchange);
        } catch (TimeoutException ex) {

        }
        return null;
    }

    /**
     * Validates the compulsary parameters. If those parameters are not available, it throws the error.
     * @param context
     * @throws IOException
     * @throws InterruptedException
     */
    @Override
    public void checkOutputSpecs(JobContext context) throws IOException, InterruptedException {
        String rabbitmqServers = context.getConfiguration().get(RABBITMQ_SERVERS);
        if (rabbitmqServers == null || rabbitmqServers.isEmpty()) {
            throw new IOException(RABBITMQ_SERVERS + " is not set in configuration");
        }

        String exchangeName = context.getConfiguration().get(RABBITMQ_EXCHANGE);
        if (exchangeName == null || exchangeName.isEmpty()) {
            throw new IOException(RABBITMQ_EXCHANGE + " is not set in configuration");
        }

    }

    @Override
    public OutputCommitter getOutputCommitter(TaskAttemptContext context) throws IOException, InterruptedException {
        return new OutputCommitter() {
            @Override
            public void setupJob(JobContext jobContext) throws IOException {

            }

            @Override
            public void setupTask(TaskAttemptContext taskContext) throws IOException {

            }

            @Override
            public boolean needsTaskCommit(TaskAttemptContext taskContext) throws IOException {
                return false;
            }

            @Override
            public void commitTask(TaskAttemptContext taskContext) throws IOException {

            }

            @Override
            public void abortTask(TaskAttemptContext taskContext) throws IOException {

            }
        };
    }
}
