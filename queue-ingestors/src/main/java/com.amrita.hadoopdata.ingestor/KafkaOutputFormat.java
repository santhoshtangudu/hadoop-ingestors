package com.amrita.hadoopdata.ingestor;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.mapreduce.*;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;

import java.io.IOException;
import java.util.Properties;

/**
 * KafkaOutputFormat is the class which helps to ingest data to Kafka. This class should set as OutputFormat in Hadoop
 * MapReduce Job to redirect the messages to Kafka.
 * @param <K> Key (of type K) is being used to decide the partition id in the topic to which the message should forward to
 * @param <V> Value (of type V) is actual message
 */
public class KafkaOutputFormat<K, V> extends OutputFormat<K, V> {
    private static final String KAFKA_BOOTSTRAP_SERVERS = "mapred.kafka.bootstrap.servers";
    private static final String KAFKA_TOPIC = "mapred.kafka.topic";
    private static final String KAFKA_CLIENT_ID = "mapred.kafka.client.id";
    private static final String KAFKA_ACK_STRATEGY = "mapred.kafka.ack";
    private static final String KAFKA_RETRIES = "mapred.kafka.retries";
    private static final String KAFKA_LINGER_MS = "mapred.kafka.linger.ms";
    private static final String KAFKA_KEY_SERIALIZER = "mapred.kafka.key.serializer";
    private static final String KAFKA_VALUE_SERIALIZER = "mapred.kafka.value.serializer";
    private static final String KAFKA_CUSTOM_PARTITIONER = "mapred.kafka.custom.partitioner";

    /**
     * Returns KafkaRecordWriter object. Kafka Producer expects some parameters to be set. If those values are not there,
     * the default values are assigned here. Here, user given parameters overwrite the default parameters.
     * @param context
     * @return
     * @throws IOException
     * @throws InterruptedException
     */
    public RecordWriter<K, V> getRecordWriter(TaskAttemptContext context) throws IOException, InterruptedException {
        Configuration conf = context.getConfiguration();

        //Read the configuration from the user
        String kafkaBootstrapServers = conf.get(KAFKA_BOOTSTRAP_SERVERS);
        String kafkaTopic = conf.get(KAFKA_TOPIC);
        String kafkaClientID = conf.get(KAFKA_CLIENT_ID, "KafkaIngestor");
        String kafkaAckStrategy = conf.get(KAFKA_ACK_STRATEGY, "1");
        String kafkaRetries = conf.get(KAFKA_RETRIES, "0");
        String kafkaLinerMS = conf.get(KAFKA_LINGER_MS, "0");

        //Assume the key and value is in String format. Thats why StringSerializer has been used. Depends on the usecase,
        //change these serializers
        String keySerializer = conf.get(KAFKA_KEY_SERIALIZER, StringSerializer.class.getName());
        String valueSerializer = conf.get(KAFKA_VALUE_SERIALIZER, StringSerializer.class.getName());

        //Create KafkaProducer
        Properties props = new Properties();
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaBootstrapServers);
        props.put(ProducerConfig.CLIENT_ID_CONFIG, kafkaClientID);
        props.put(ProducerConfig.ACKS_CONFIG, kafkaAckStrategy);
        props.put(ProducerConfig.RETRIES_CONFIG, Integer.parseInt(kafkaRetries));
        props.put(ProducerConfig.LINGER_MS_CONFIG, Integer.parseInt(kafkaLinerMS));

        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, keySerializer);
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, valueSerializer);

        if(conf.get(KAFKA_CUSTOM_PARTITIONER) != null && !conf.get(KAFKA_CUSTOM_PARTITIONER).isEmpty()) {
            props.put(ProducerConfig.PARTITIONER_CLASS_CONFIG, conf.get(KAFKA_CUSTOM_PARTITIONER));
        }

        KafkaProducer kafkaProducer =  new KafkaProducer<Long, String>(props);

        return new KafkaRecordWriter(kafkaProducer, kafkaTopic);
    }

    /**
     * Validates the compulsary parameters. If those parameters are not available, it throws the error.
     * @param jobContext
     * @throws IOException
     * @throws InterruptedException
     */
    public void checkOutputSpecs(JobContext jobContext) throws IOException, InterruptedException {
        String bootstrapServers = jobContext.getConfiguration().get(KAFKA_BOOTSTRAP_SERVERS);
        if (bootstrapServers == null || bootstrapServers.isEmpty()) {
            throw new IOException(KAFKA_BOOTSTRAP_SERVERS + " is not set in configuration");
        }

        String topicName = jobContext.getConfiguration().get(KAFKA_TOPIC);
        if (topicName == null || topicName.isEmpty()) {
            throw new IOException(KAFKA_TOPIC + " is not set in configuration");
        }
    }

    public OutputCommitter getOutputCommitter(TaskAttemptContext context) throws IOException, InterruptedException {
        return new OutputCommitter() {
            @Override
            public void setupJob(JobContext jobContext) throws IOException {

            }

            @Override
            public void setupTask(TaskAttemptContext taskContext) throws IOException {

            }

            @Override
            public boolean needsTaskCommit(TaskAttemptContext taskContext) throws IOException {
                return false;
            }

            @Override
            public void commitTask(TaskAttemptContext taskContext) throws IOException {

            }

            @Override
            public void abortTask(TaskAttemptContext taskContext) throws IOException {

            }
        };
    }
}
