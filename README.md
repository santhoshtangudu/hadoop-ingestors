
  Hadoop-Ingestors
==============

Hadoop-Ingestors: Contains a library queue-ingestors which contains Hadoop OutputFormats to ingest data from HDFS to Distributed Message Brokers using MapReduce Jobs.

This library currently supports following distributed message brokers:
   - RabbitMQ
   - Kafka  

Note that the library queue-ingestors is primarily intended for developers with experience in using Hadoop.

For examples of how to use queue-ingestors as a library to ingest data to these queues using Hadoop MapReduce jobs, see the queue-ingestors-examples/ directory

------------
Dependencies
------------

Hadoop. Any recent version should work. Hadoop is a "provided" dependency. You can use any recent version of Hadoop in POM file
Apart from these, this library depends on distributed message broker clients to ingest data to respective queues.

------------
Installation
------------

You'll have to build the queue-ingestors library using maven. It depends on maven to build the library. Run the following commands to build the library.

### Build

Build Queue-Ingestors with the following command:

    mvn clean package -DskipTests
   
It will create two files:

    target/queue-ingestors-X.Y-SNAPSHOT.jar
    target/queue-ingestors-X.Y-SNAPSHOT-jar-with-dependencies.jar

-------------
Library usage
-------------

The library queue-ingestors provides standard set of Hadoop OutputFormats and RecordWriter classes to ingest data to Distributed Message Brokers. 
 
The output formats available in this library are as follows.

     	KafkaOutputFormat
        RabbitMQOutputFormat

The properties that can be set on these output formats are summarized in the table below:

|Format|Property|Default|Mandatory|Description|
|------|--------|-------|-----------|
|`KafkaOutputFormat`|`mapred.kafka.bootstrap.servers`|`-`|`Yes`|KafkaCluster Bootstrap Server to connect and ingest the messages.|
||`mapred.kafka.topic`|`-` |`Yes`| Kafka Topic Name|
||`mapred.kafka.client.id`|`KafkaIngestor`|`No`| Kafka Client ID|
||`mapred.kafka.ack`|`1`|`No`|Equivalent to acks config in Kafka Producer. There are three possible values 0,1,all. Read Kafka documentation for more details on acks.|
||`mapred.kafka.retries`|`0`|`No`|Equivalent to retries config in Kafka Producer. This has the potential to disturb the ordering guaratees. Read Kafka documentation for more details on retries.|
||`mapred.kafka.linger.ms`|`0`|`No`|Equivalent to linger.ms config in Kafka Producer. This setting helps producer to enable the batching. Read Kafka documentation for more details on linger.ms.|
||`mapred.kafka.key.serializer`|`StringSerializer`|`No`|Equivalent to key.serializer config in Kafka Producer. Depends on the key type, user can set the serializer. Read Kafka documentation for more details on key.serializer||
||`mapred.kafka.value.serializer`|`StringSerializer`|`No`|Equivalent to value.serializer config in Kafka Producer. Depends on the value type, user can set the serializer. Read Kafka documentation for more details on value.serializer|
||`mapred.kafka.custom.partitioner`|`Round Robin` | `No`| Equivalent to partitioner.class class config in Kafka Producer. If there is any custom partitioner given by the user, it will override the default value.|
|`RabbitMQOutputFormat`|`mapred.rabbitmq.servers`|`-`|`Yes`| RabbitMQ message broker to which the MapReduce job should connect to.|
||`mapred.rabbitmq.exchange`|`-` |`Yes`| RabbitMQ Exchange Name|
||`mapred.rabbitmq.exchange.type`|`-`|`Yes`| RabbitMQ Exchange type. Exchanges are various types like FANOUT, DIRECT, TOPIC. Refer RabbitMQ documentation for more details on exchange types.|

For examples on how to use this library in your own projects, see the `queue-ingestors-examples/` folder. These examples help us to understand this library to ingest data from HDFS to Kafka/ RabbitMQ.
We have used string-db protein interactions to ingest the data to Kafka/RabbitMQ. Here we have defined the Custom Partitioner to decide the partition id in the topic. For RabbitMQ, producer doesn't aware of the queues subscribed for this usecase. 

------------------------------
Hadoop MapReduce Job Execution
------------------------------ 

Run following command along with the required parameters

	hadoop jar target/queue-ingestors-examples <CLASS_WITH_MAIN_METHOD> -D<CONFIG_PARAM>=<CONFIG_VALUE> <input_path>

Here, give the class name which we need to execute CLASS_WITH_MAIN_METHOD (mapreduce job), pass the required CONFIG_PARAMs and its values CONFIG_VALUEs and pass the input data location. 

